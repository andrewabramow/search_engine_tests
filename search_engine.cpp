﻿// search_engine.cpp : Defines the entry point for the application.
//

#include <iostream>
#include <string>
#include <vector>
#include "gtest/gtest.h"
#include "search_engine.h"
#include "JSON.h"
#include "InvertIndex.h"
#include "Server.h"

#ifdef TESTS 
int main()
{
	//  JSON part:
	ConverterJSON newJSON;

	//  list of requests
	for (auto& el : newJSON.GetRequests()) 
	{
		std::cout << el << std::endl;
	}

	//  response limit
	std::cout << "Response limit is: " <<
		newJSON.GetResponsesLimit() << std::endl;
//________________________________________________________________

	//  Inverted index part:
	//  (pointer for SearchServer constructor)
	InvertedIndex* newInvInd = new InvertedIndex();

	//  update documents
	newInvInd->UpdateDocumentBase(newJSON.GetTextDocuments());
//________________________________________________________________

	//  Search server part:
	SearchServer* server = new SearchServer(*newInvInd);

	auto requests = newJSON.GetRequests();

	auto answers = ConvertAnswer(server->search(requests));

	newJSON.PutAnswers(answers);
//________________________________________________________________

	//  Memory clear:
	delete newInvInd;
	delete server;
	//return RUN_ALL_TESTS();
}
#endif
